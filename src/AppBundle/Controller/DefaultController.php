<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Code;
use AppBundle\Form\Type\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $status = null;
        $region = null;

        $form = $this->createForm( SearchType::class, NULL);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $post_code = $form['search']->getData();
                $data = $this->selectAction($post_code);
                $region = $data[1];
                $status = $data[0];
            }
        }
        //$post_code = 'M469XE';
        return $this->render('default/index.html.twig',
            ['form' => $form->createView(), 'region'=>$region, 'status'=>$status]
        );
    }

    public function selectAction($post_code)
    {
        $data = [];
        $code = $this->getDoctrine()->getRepository('AppBundle:Code')->findOneBy(['postcode'=>$post_code]);

        if(!$code){
            $url = "api.postcodes.io/postcodes/".$post_code;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            $data_r = json_decode($response);
            if($data_r->{'status'} != 200){
                array_push($data, 'Invalid postcode', null, null);
                return $data;
            }
            $postcode = $data_r->{'result'}->{'postcode'};
            $region = $data_r->{'result'}->{'region'};
            $this->saveAction($postcode, $region);
            array_push($data, 'api base', $postcode, $region);

            return $data;
        }
        array_push($data, 'local base', $code->getPostcode(), $code->getRegion());

        return $data;
    }

    public function saveAction($postcode, $region){

        $code = new Code();

        $code->setPostcode($postcode);
        $code->setRegion($region);
        $code->setCreatedAt(new \DateTime("now"));
        $code->setUpdatedAt(new \DateTime("now"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($code);
        $em->flush();

        return true;
    }
}
